package com.example.u6redestelefonia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Activity0 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_0);
    }
    //---------------------------------------------------------------------------------------------- Retornar a MAIN_ACTIVITY
    public void retornar(View view){
        finish();
    }
    //---------------------------------------------------------------------------------------------- Enviar a ACTIVITY_1
    public void siguiente(View view){
        Intent intent = new Intent(this,Activity1.class);
        startActivity(intent);
    }
}