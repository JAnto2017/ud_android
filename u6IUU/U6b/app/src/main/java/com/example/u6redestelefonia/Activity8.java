package com.example.u6redestelefonia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Activity8 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_8);
    }
    //---------------------------------------------------------------------------------------------- enviar a: MAIN ACTIVITY
    public void retornar (View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}