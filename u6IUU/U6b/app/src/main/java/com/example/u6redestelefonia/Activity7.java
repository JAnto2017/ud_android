package com.example.u6redestelefonia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Activity7 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_7);
    }

    //---------------------------------------------------------------------------------------------- retornar a: ACTIVITY_6
    public void anterior(View view) {
        finish();
    }
    //---------------------------------------------------------------------------------------------- enviar a: ACTIVITY_8
    public void enviar(View view) {
        Intent intent = new Intent(this,Activity8.class);
        startActivity(intent);
    }
}