package com.example.u6redestelefonia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    //---------------------------------------------------------------------------------------------- Enviar a Modelo de Red (Activity_0)
    public void modRed(View view){
        Intent intent = new Intent(this,Activity0.class);
        startActivity(intent);
    }

    //---------------------------------------------------------------------------------------------- Enviar a Interfaces Física de Acceso (Activity_6)
    public void intFisAcc(View view){
        Intent intent = new Intent(this,Activity6.class);
        startActivity(intent);
    }

    //---------------------------------------------------------------------------------------------- Enviar a Funciones de Centralitas PBX (Activity_9)
    public void centralPBX(View view){
        Intent intent = new Intent(this, Activity9.class);
        startActivity(intent);
    }

    //---------------------------------------------------------------------------------------------- Enviar a Cuestionario (Activiti_A)
    public void cuestion(View view){
        Intent intent = new Intent(this, Activity_A.class);
        startActivity(intent);
    }
}