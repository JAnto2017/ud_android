package com.example.u6redestelefonia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Activity1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);
    }
    //---------------------------------------------------------------------------------------------- Retornar a Activity_0
    public void retornar(View view){
        finish();
    }
    //---------------------------------------------------------------------------------------------- Eviar a Activity_2
    public void siguiente(View view){
        Intent intent = new Intent(this,Activity2.class);
        startActivity(intent);
    }
}