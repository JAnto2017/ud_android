package com.example.u6redestelefonia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
    }

    //---------------------------------------------------------------------------------------------- retornar a: ACTIVITY_1
    public void retornar(View view){
        finish();
    }
    //---------------------------------------------------------------------------------------------- avanzar hacia: ACTIVITY_3
    public void avanzar(View view){
        Intent intent = new Intent(this,Activity3.class);
        startActivity(intent);
    }
}