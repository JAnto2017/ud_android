package com.example.u6redestelefonia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Activity5 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_5);
    }
    //---------------------------------------------------------------------------------------------- enviar a: MAIN_ACTIVITY
    public void inicio(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}