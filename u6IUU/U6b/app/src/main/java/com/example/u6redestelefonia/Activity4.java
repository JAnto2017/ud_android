package com.example.u6redestelefonia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Activity4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_4);
    }
    //---------------------------------------------------------------------------------------------- retornar a: ACTIVITY_3
    public void retornar(View view){
        finish();
    }
    //---------------------------------------------------------------------------------------------- enviar a: ACTIVITY_5
    public void siguiente(View view){
        Intent intent = new Intent(this,Activity5.class);
        startActivity(intent);
    }
}