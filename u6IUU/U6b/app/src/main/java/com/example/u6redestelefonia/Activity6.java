package com.example.u6redestelefonia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Activity6 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_6);
    }
    //---------------------------------------------------------------------------------------------- Retornar a: ACTIVITY MAIN (Inicio)
    public void retornar(View view){
        finish();
    }
    //---------------------------------------------------------------------------------------------- Envíar a: ACTIVITY_7
    public void siguiente(View view){
        Intent intent = new Intent(this,Activity7.class);
        startActivity(intent);
    }
}