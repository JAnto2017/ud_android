package com.example.u6redestelefonia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

public class Activity_A extends AppCompatActivity {

    private String email = "elee0109.bt@gmail.com";
    private String asunto;
    private String mensaje;
    private int aciertos;
    private int errores;

    //Declaración de los RadioButton - CORRECTOS - en las 13 respuestas
    private RadioButton rb_a2;
    private RadioButton rb_a5;
    private RadioButton rb_a6;
    private RadioButton rb_a12;
    private RadioButton rb_a13;
    private RadioButton rb_a18;
    private RadioButton rb_a21;
    private RadioButton rb_a23;
    private RadioButton rb_a26;
    private RadioButton rb_a28;
    private RadioButton rb_a33;
    private RadioButton rb_a36;
    private RadioButton rb_a38;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a);

        //Aquí es donde se relacionan las variables declaradas con las soluciones de las preguntas del test
        //Se deben cambiar los id para los radiobutton que sena  Correctos como respueta
        rb_a2 = (RadioButton) findViewById(R.id.rb_a2);
        rb_a5 = (RadioButton) findViewById(R.id.rb_a5);
        rb_a6 = (RadioButton) findViewById(R.id.rb_a6);
        rb_a12 = (RadioButton) findViewById(R.id.rb_a12);
        rb_a13= (RadioButton) findViewById(R.id.rb_a13);
        rb_a18= (RadioButton) findViewById(R.id.rb_a18);
        rb_a21= (RadioButton) findViewById(R.id.rb_a21);
        rb_a23= (RadioButton) findViewById(R.id.rb_a23);
        rb_a26= (RadioButton) findViewById(R.id.rb_a26);
        rb_a28= (RadioButton) findViewById(R.id.rb_a28);
        rb_a33= (RadioButton) findViewById(R.id.rb_a33);
        rb_a36= (RadioButton) findViewById(R.id.rb_a36);
        rb_a38= (RadioButton) findViewById(R.id.rb_a38);
    }

    //---------------------------------------------------------------------------------------------- Determinar las soluciones del test
    public void comprobar(View view) {

        aciertos = 0;
        errores = 0;

        //Comprobaciones de lo seleccionado
        //pregunta nº1
        if (rb_a2.isChecked()){
            aciertos+=1;
            rb_a2.setBackgroundColor(Color.rgb(60,224,16));
        }else {
            errores+=1;
            //rb_a2.setBackgroundColor(Color.rgb(224,16,60));
        }
        //pregunta nº2
        if (rb_a5.isChecked()){
            aciertos+=1;
            rb_a5.setBackgroundColor(Color.rgb(60,224,16));
        }else {
            errores+=1;
            //rb_a5.setBackgroundColor(Color.rgb(224,16,60));
        }
        //pregunta nº3
        if (rb_a6.isChecked()){
            aciertos+=1;
            rb_a6.setBackgroundColor(Color.rgb(60,224,16));
        }else {
            errores+=1;
            //rb_a6.setBackgroundColor(Color.rgb(224,16,60));
        }
        //pregunta nº4
        if (rb_a12.isChecked()){
            aciertos+=1;
            rb_a12.setBackgroundColor(Color.rgb(60,224,16));
        }else {
            errores+=1;
            //rb_a12.setBackgroundColor(Color.rgb(224,16,60));
        }
        //pregunta nº5
        if (rb_a13.isChecked()){
            aciertos+=1;
            rb_a13.setBackgroundColor(Color.rgb(60,224,16));
        }else{
            errores+=1;
            //rb_a13.setBackgroundColor(Color.rgb(224,16,60));
        }
        //pregunta nº6
        if (rb_a18.isChecked()){
            aciertos+=1;
            rb_a18.setBackgroundColor(Color.rgb(60,224,16));
        }else{
            errores+=1;
            //rb_a18.setBackgroundColor(Color.rgb(224,16,60));
        }
        //pregunta nº7
        if (rb_a21.isChecked()){
            aciertos+=1;
            rb_a21.setBackgroundColor(Color.rgb(60,224,16));
        }else {
            errores+=1;
            //rb_a21.setBackgroundColor(Color.rgb(224,16,60));
        }
        //pregunta nº8
        if (rb_a23.isChecked()){
            aciertos+=1;
            rb_a23.setBackgroundColor(Color.rgb(60,224,16));
        }else{
            errores+=1;
            //rb_a23.setBackgroundColor(Color.rgb(224,16,60));
        }
        //pregunta nº9
        if (rb_a26.isChecked()){
            aciertos+=1;
            rb_a26.setBackgroundColor(Color.rgb(60,224,16));
        }else{
            errores+=1;
            //rb_a26.setBackgroundColor(Color.rgb(224,16,60));
        }
        //pregunta nº10
        if (rb_a28.isChecked()){
            aciertos+=1;
            rb_a28.setBackgroundColor(Color.rgb(60,224,16));
        }else{
            errores+=1;
            //rb_a28.setBackgroundColor(Color.rgb(224,16,60));
        }
        //pregunta nº11
        if (rb_a33.isChecked()){
            aciertos+=1;
            rb_a33.setBackgroundColor(Color.rgb(60,224,16));
        }else{
            errores+=1;
            //rb_a33.setBackgroundColor(Color.rgb(224,16,60));
        }
        //pregunta nº12
        if (rb_a36.isChecked()){
            aciertos+=1;
            rb_a36.setBackgroundColor(Color.rgb(60,224,16));
        }else{
            errores+=1;
            //rb_a36.setBackgroundColor(Color.rgb(224,16,60));
        }
        //pregunta nº13
        if (rb_a38.isChecked()){
            aciertos+=1;
            rb_a38.setBackgroundColor(Color.rgb(60,224,16));
        }else{
            errores+=1;
            //rb_a38.setBackgroundColor(Color.rgb(224,16,60));
        }

        setAciertos(aciertos);
        setErrores(errores);

        //Mensaje de notificación aciertos y errores
        Toast toast = Toast.makeText(this,"Aciertos: "+aciertos+" Errores: "+errores,Toast.LENGTH_LONG);
        toast.show();

    }

    //---------------------------------------------------------------------------------------------- Btn Enviar por Email
    public void enviar (View view) {

        asunto = "test U8";
        mensaje = "Aciertos: "+getAciertos()+" Errores: "+getErrores();

        //Envío de email
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        intent.putExtra(Intent.EXTRA_SUBJECT,asunto);
        intent.putExtra(Intent.EXTRA_TEXT,mensaje);

        //establecemos tipo de Intent
        intent.setType("message/rfc822");

        if (getAciertos()>13) {
            //lanzamos el selctor de cliente de correo
            startActivity(Intent.createChooser(intent,"Gmail"));
        }
    }

    public void setAciertos(int _a){
        this.aciertos = _a;
    }

    public int getAciertos(){
        return aciertos;
    }

    public void setErrores(int _e){
        this.errores = _e;
    }

    public int getErrores(){
        return errores;
    }
}