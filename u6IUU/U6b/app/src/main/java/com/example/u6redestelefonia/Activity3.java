package com.example.u6redestelefonia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Activity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);
    }

    //---------------------------------------------------------------------------------------------- retornar a: ACTIVITY_2
    public void retornar(View view){
        finish();
    }
    //---------------------------------------------------------------------------------------------- avanzar a: ACTIVITY_4
    public void siguiente(View view){
        Intent intent = new Intent(this,Activity4.class);
        startActivity(intent);
    }
}