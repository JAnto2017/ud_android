package com.example.u6redestelefonia;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class Activity9 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_9);
    }

    //---------------------------------------------------------------------------------------------- inviar a inicio (Main Activity)
    public void inicio(View view) {
        finish();
    }
}