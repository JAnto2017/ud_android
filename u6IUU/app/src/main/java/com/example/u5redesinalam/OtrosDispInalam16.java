package com.example.u5redesinalam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class OtrosDispInalam16 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otros_disp_inalam16);
    }
    //---------------------------------------------------------------------------------------------- Retornar al principal
    public void saltInic(View view){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}