package com.example.u5redesinalam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AdaptadorInalamb14 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adaptador_inalamb14);
    }
    //---------------------------------------------------------------------------------------------- Evía a: Antenas Wi-Fi
    public void antWifi(View view){
        Intent intent = new Intent(this,AntenasWiFi15.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- Salir
    public void salir(View view){
        finish();
    }
}