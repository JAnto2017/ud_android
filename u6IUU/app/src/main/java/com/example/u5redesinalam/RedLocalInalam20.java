package com.example.u5redesinalam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class RedLocalInalam20 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_red_local_inalam20);
    }
    //---------------------------------------------------------------------------------------------- Envíar a: Monitorización de redes inalámbricas
    public void monitRedIna(View view){
        Intent intent = new Intent(this,MonitorizarRedIna21.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- salir
    public void salir(View view){
        finish();
    }
}