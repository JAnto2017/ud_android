package com.example.u5redesinalam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class SistUnivTelecoMob31 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sist_univ_teleco_mob31);
    }
    //---------------------------------------------------------------------------------------------- Envíar a: Conexión a internet vía satélite
    public void conIntViaSat(View view){
        Intent intent = new Intent(this, ConexViaSat32.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- Anterior
    public void salir(View view){
        finish();
    }
}