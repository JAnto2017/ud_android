package com.example.u5redesinalam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class InternetMovil03 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internet_movil03);
    }
    //---------------------------------------------------------------------------------------------- Enviar a: Sistema universal de Telecomunicaciones Móviles
    public void sistUniTelMov(View view){
        Intent intent = new Intent(this,SistUnivTelecoMob31.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- Anterior
    public void anterior(View view){
        finish();
    }
}