package com.example.u5redesinalam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class EquipRedWiFi10 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equip_red_wi_fi10);
    }
    //---------------------------------------------------------------------------------------------- Envíar a: Puntos de acceso AP
    public void puntAcc(View view){
        Intent intent = new Intent(this,PuntoAcceso11.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- Salir
    public void salir(View view){
        finish();
    }
}