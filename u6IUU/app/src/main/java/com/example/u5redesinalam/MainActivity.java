package com.example.u5redesinalam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        //getActionBar().hide();
    }
    //---------------------------------------------------------------------------------------------- Equipos de redes inalámbricas
    public void equipRedInalam(View view){
        Intent inte1 = new Intent(this,EquipRedWiFi10.class);
        startActivity(inte1);
    }
    //---------------------------------------------------------------------------------------------- Redes locales inalámbricas
    public void redLocInalam(View view){
        Intent inte2 = new Intent(this, RedLocalInalam20.class);
        startActivity(inte2);
    }
    //---------------------------------------------------------------------------------------------- Internet móvil
    public void InterMov(View view){
        Intent inte3 = new Intent(this,InternetMovil03.class);
        startActivity(inte3);
    }
    //---------------------------------------------------------------------------------------------- Cuestionario
    public void Cuestio(View view){
        Intent inte4 = new Intent(this, Cuestionario.class);
        startActivity(inte4);
    }
}