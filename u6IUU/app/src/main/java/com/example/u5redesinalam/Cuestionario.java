package com.example.u5redesinalam;

import androidx.annotation.ColorInt;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

public class Cuestionario extends AppCompatActivity {

    private int errores,aciertos;
    private RadioButton rb1,rb2,rb3,rb4,rb5,rb6,rb7,rb8,rb9,rb10,rb11,rb12,rb13,rb14;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuestionario);

        //------------------------------------------------------------------------------------------ enlace con RadioButton que son True
        rb1 = (RadioButton)findViewById(R.id.rdb3);
        rb2 = (RadioButton)findViewById(R.id.rdb7);
        rb3 = (RadioButton)findViewById(R.id.rdb10);
        rb4 = (RadioButton)findViewById(R.id.rdb14);
        rb5 = (RadioButton)findViewById(R.id.rdb19);
        rb6 = (RadioButton)findViewById(R.id.rdb23);
        rb7 = (RadioButton)findViewById(R.id.rdb24);
        rb8 = (RadioButton)findViewById(R.id.rdb27);
        rb9 = (RadioButton)findViewById(R.id.rdb33);
        rb10= (RadioButton)findViewById(R.id.rdb37);
        rb11= (RadioButton)findViewById(R.id.rdb38);
        rb12= (RadioButton)findViewById(R.id.rdb42);
        rb13= (RadioButton)findViewById(R.id.rdb47);
        rb14= (RadioButton)findViewById(R.id.rdb48);
    }
    //---------------------------------------------------------------------------------------------- Comprobaciones Button
    public void comprobar(View view){
        errores = 0;
        aciertos = 0;

        if (rb1.isChecked()==true){
            aciertos += 1;
            rb1.setBackgroundColor(Color.argb(145,131,235,198));
        }else{
            errores += 1;
        }
        if (rb2.isChecked()==true){
            aciertos += 1;
            rb2.setBackgroundColor(Color.argb(145,131,235,198));
        }else{
            errores +=1;
        }
        if (rb3.isChecked()==true){
            aciertos += 1;
            rb3.setBackgroundColor(Color.argb(145,131,235,198));
        }else{
            errores += 1;
        }
        if (rb4.isChecked()==true){
            aciertos += 1;
            rb4.setBackgroundColor(Color.argb(145,131,235,198));
        }else{
            errores += 1;
        }
        if (rb5.isChecked()==true){
            aciertos += 1;
            rb4.setBackgroundColor(Color.argb(145,131,235,198));
        }else {
            errores += 1;
        }
        if (rb6.isChecked()==true){
            aciertos += 1;
            rb6.setBackgroundColor(Color.argb(145,131,235,198));
        }else {
            errores += 1;
        }
        if (rb7.isChecked()==true){
            aciertos += 1;
            rb7.setBackgroundColor(Color.argb(145,131,235,198));
        }else{
            errores += 1;
        }
        if (rb8.isChecked()==true){
            aciertos += 1;
            rb8.setBackgroundColor(Color.argb(145,131,235,198));
        }else{
            errores += 1;
        }
        if (rb9.isChecked()==true){
            aciertos += 1;
            rb9.setBackgroundColor(Color.argb(145,131,235,198));
        }else{
            errores += 1;
        }
        if (rb10.isChecked()==true){
            aciertos += 1;
            rb10.setBackgroundColor(Color.argb(145,131,235,198));
        }else{
            errores += 1;
        }
        if (rb11.isChecked()==true){
            aciertos += 1;
            rb11.setBackgroundColor(Color.argb(145,131,235,198));
        }else {
            errores += 1;
        }
        if (rb12.isChecked()==true){
            aciertos += 1;
            rb12.setBackgroundColor(Color.argb(145,131,235,198));
        }else {
            errores += 1;
        }
        if (rb13.isChecked()==true){
            aciertos += 1;
            rb13.setBackgroundColor(Color.argb(145,131,235,198));
        }else {
            errores += 1;
        }
        if (rb14.isChecked()==true){
            aciertos += 1;
            rb14.setBackgroundColor(Color.argb(145,131,235,198));
        }else {
            errores += 1;
        }

        Toast notificaAciertos = Toast.makeText(this,"Aciertos: "+aciertos+" Errores: "+errores,Toast.LENGTH_LONG);
        notificaAciertos.show();

    }

}