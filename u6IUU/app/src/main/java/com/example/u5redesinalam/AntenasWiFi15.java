package com.example.u5redesinalam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AntenasWiFi15 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_antenas_wi_fi15);
    }
    //---------------------------------------------------------------------------------------------- Envíar a: Otros dispositivos inalámbricos
    public void otroDispIna(View view){
        Intent intent = new Intent(this, OtrosDispInalam16.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- salir
    public void salir(View view){
        finish();
    }
}