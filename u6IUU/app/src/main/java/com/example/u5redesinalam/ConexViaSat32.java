package com.example.u5redesinalam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ConexViaSat32 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conex_via_sat32);
    }
    //----------------------------------------------------------------------------------------------Retornar a Main-Activity
    public void ret_EquipRedIna(View view){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}