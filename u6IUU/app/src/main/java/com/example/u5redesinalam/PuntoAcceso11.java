package com.example.u5redesinalam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class PuntoAcceso11 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_punto_acceso11);
    }
    //---------------------------------------------------------------------------------------------- Envía a siguiente - Aplicar seguridad al punto de acceso
    public void aplicSegPunAcc(View view){
        Intent intent =  new Intent(this, AplicarSeguridad12.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- salir
    public void salir(View view){
        finish();
    }
}