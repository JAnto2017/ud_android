package com.example.u5redesinalam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ConfiguraRoute13 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configura_route13);
    }
    //---------------------------------------------------------------------------------------------- Envíar - Adaptadores inalámbricos
    public void adapInal(View view){
        Intent intent = new Intent(this, AdaptadorInalamb14.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- Salir
    public void salir(View view){
        finish();
    }
}