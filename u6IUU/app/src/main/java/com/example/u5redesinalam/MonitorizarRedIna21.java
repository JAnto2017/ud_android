package com.example.u5redesinalam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MonitorizarRedIna21 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitorizar_red_ina21);
    }
    //---------------------------------------------------------------------------------------------- Retornar a inicio
    public void ret_inic(View view){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }

}