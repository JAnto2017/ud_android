package com.example.u5redesinalam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AplicarSeguridad12 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aplicar_seguridad12);
    }
    //---------------------------------------------------------------------------------------------- Envíar - Configurar el Router
    public void confRouter(View view){
        Intent intent = new Intent(this, ConfiguraRoute13.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- Salir
    public void salir(View view){
        finish();
    }
}